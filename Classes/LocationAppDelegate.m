//
//  LocationAppDelegate.m
//  Location
//
//  Created by admin on 2/15/13.
//  Copyright __MyCompanyName__ 2013. All rights reserved.
//

#import "LocationAppDelegate.h"
#import "LocationViewController.h"

@implementation LocationAppDelegate

@synthesize window;
@synthesize viewController;


- (void)applicationDidFinishLaunching:(UIApplication *)application {    
    
    // Override point for customization after app launch    
    [window addSubview:viewController.view];
    [window makeKeyAndVisible];
}


- (void)dealloc {
    [viewController release];
    [window release];
    [super dealloc];
}


@end
