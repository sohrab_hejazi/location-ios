//
//  LocationAppDelegate.h
//  Location
//
//  Created by admin on 2/15/13.
//  Copyright __MyCompanyName__ 2013. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LocationViewController;

@interface LocationAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    LocationViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet LocationViewController *viewController;

@end

