//
//  LocationViewController.h
//  Location
//  sohrab 
//  Created by admin on 2/15/13.
//  Copyright __MyCompanyName__ 2013. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface LocationViewController : UIViewController {
	IBOutlet UITextField *fname,*lname;
	IBOutlet UITextField *LONGITUDE;
	IBOutlet UITextField *LATITUDE;
	IBOutlet UITextField *ALTITUDE;
	IBOutlet UITextField *HORIZENTALACCURACY;
	IBOutlet UITextField *VERTICALACCURACY;
	CLLocationManager *locman;
	IBOutlet UIWebView * webview;
	IBOutlet UIView  *waitView;
	IBOutlet UIButton  *backBtn;
}
@property(nonatomic,retain) IBOutlet UIButton  *backBtn;
@property (assign, nonatomic) CLLocationManager *locman;
@property (retain, nonatomic) UIView *waitView;
@property(nonatomic, retain) UIWebView *webview;
@property (retain, nonatomic) IBOutlet UITextField *fname;
@property (retain, nonatomic) IBOutlet UITextField *LONGITUDE;
@property (retain, nonatomic) IBOutlet UITextField *LATITUDE;
@property (retain, nonatomic) IBOutlet UITextField *ALTITUDE;
@property (retain, nonatomic) IBOutlet UITextField *HORIZENTALACCURACY;
@property (retain, nonatomic) IBOutlet UITextField *VERTICALACCURACY;
//@property (nonatomic, copy) void(^completionHandler)(NSURLResponse *response, NSData *data, NSError *error);
//@property (retain,nonatomic) IBoutlet UITextField *name;
//@property (retain,nonatomic) IBoutlet UITextField *gspNumber;
-(IBAction) FindLoc: (id)sender;
-(IBAction) comeBack: (id)sender;
-(IBAction) SendToServer: (id)sender;
- (IBAction)textFieldReturn:(id)sender;
- (IBAction)backgroundTouched:(id)sender;@end

