//
//  LocationViewController.m
//  Location
//
//  Created by admin on 2/15/13.
//  Copyright __MyCompanyName__ 2013. All rights reserved.
//

#import "LocationViewController.h"


@implementation LocationViewController
@synthesize locman;
@synthesize webview,backBtn;
@synthesize fname;
@synthesize LONGITUDE,LATITUDE,ALTITUDE;
@synthesize HORIZENTALACCURACY,VERTICALACCURACY;



-(IBAction)textFieldReturn:(id)sender
{
	[sender resignFirstResponder];
} 

-(IBAction) comeBack:(id)sender
{
	webview.hidden=YES;
	backBtn.hidden=YES;
}
-(IBAction)backgroundTouched:(id)sender
{
	[fname resignFirstResponder];
	[ LONGITUDE resignFirstResponder];
	[LATITUDE resignFirstResponder];	
	[ALTITUDE resignFirstResponder];
	[HORIZENTALACCURACY resignFirstResponder];	
	[VERTICALACCURACY resignFirstResponder];	
}
-(IBAction) FindLoc: (id)sender{
	waitView.hidden = noErr;
	
	locman = [[CLLocationManager alloc] init];
    locman.delegate = self;
    locman.desiredAccuracy = kCLLocationAccuracyThreeKilometers;
    //locman.distanceFilter = 10;
    [locman startUpdatingLocation];
	
	//waitView.hidden= YES;	
}
-(IBAction) SendToServer: (id)sender{
	    waitView.hidden = noErr;	
		webview.hidden=noErr;
	backBtn.hidden=noErr;
	        
		   // NSString *tel = [[NSUserDefaults standardUserDefaults] stringForKey:@"SBFormattedPhoneNumber"];	
		   //NSString *tel = [[UIDevice currentDevice] name];
		
	   NSString *tel = [[UIDevice currentDevice] uniqueIdentifier];		
		    NSString *Fname=fname.text;
		    Fname = [Fname stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
			
		NSString *urlString = [NSString stringWithFormat:@"http://sohrab.comxa.com/insert.php/insert.php?tel=%@&fname=%@&LONGITUDE=%@&LATITUDE=%@&ALTITUDE=%@&HORIZENTALACCURACY=%@&VERTICALACCURACY=%@",
							  tel, (Fname),(LONGITUDE.text),(LATITUDE.text), (ALTITUDE.text),(HORIZENTALACCURACY.text),(VERTICALACCURACY.text)];
		
			NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
			//NSError * e;
			//NSData	     *data = [NSURLConnection sendAsynchronousRequest:request returningResponse:nil error:&e];
		
		//[self.webview loadRequest:request];
		//return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
	/*NSHTTPURLResponse *response = nil;
	NSError *error = [[[NSError alloc] init] autorelease];
	[NSURLConnection sendAsynchronousRequest:request returningResponse:&response error:&error];*/
	NSURLConnection *conn = [NSURLConnection connectionWithRequest:request delegate:self];
    if (conn == nil) {
	   NSLog(@"cannot create connection");
   }  
		
	waitView.hidden= YES;
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    //Convert to string and append received data
    NSString *result = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
	[webview loadHTMLString:result baseURL:nil];
	
	
}
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSString *errorText;
    //Specific error
    if (error) 
        errorText = [error localizedDescription];
    //Generic error
    else
        errorText = @"An error occurred . Please check that you are connected to the Internet";
		
		//Show error
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:errorText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
		[alert release];
		
		//Hide activity indicator
		//[self clearIssuesAccessoryView];
}
- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation {
	
	LONGITUDE.text=[NSString stringWithFormat:@"%f", newLocation.coordinate.longitude];
	LATITUDE.text=[NSString stringWithFormat:@"%f", newLocation.coordinate.latitude];
	ALTITUDE.text=[NSString stringWithFormat:@"%f", newLocation.altitude];	
	HORIZENTALACCURACY.text=[NSString stringWithFormat:@"%f", newLocation.horizontalAccuracy];
	VERTICALACCURACY.text=[NSString stringWithFormat:@"%f", newLocation.verticalAccuracy];	
	
	waitView.hidden= YES;	
}


- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error {
	
    if (error.code == kCLErrorDenied) {
        // Turn off the location manager updates
        [manager stopUpdatingLocation];
        [locman release];
        locman = nil;
		waitView.hidden= YES;
    }
    //waitView.hidden = YES;
    //distanceView.hidden = NO; 
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}


- (void)viewDidLoad {
	[super viewDidLoad];
	webview.hidden=YES;
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
	[locman release];
	[fname release];
	[LONGITUDE release];
}

@end
